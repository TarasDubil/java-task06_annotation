package com.dubilok.model.annotation;

public class Person {

    @Show
    private String name = "Taras";

    @Show
    private int age = 20;
    private String email = "dubilok7@gmail.com";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

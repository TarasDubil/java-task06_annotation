package com.dubilok.model.ex_7;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.stream.Stream;

public class DogManager {

    private static Logger logger = LogManager.getLogger(DogManager.class);
    private Object object;
    private Class c;

    public DogManager(Object object) {
        this.object = object;
        this.c = object.getClass();
    }

    public void getInfoAboutClass() throws IllegalAccessException {
        logger.info("Information about class " + c.getName());
        getAllAnnotation();
        getAllFields();
        getAllConstructors();
        getAllMethods();
    }

    private void getAllFields() throws IllegalAccessException {
        logger.info("Information about field: ");
        Field[] fields = c.getDeclaredFields();
        if (fields.length == 0) {
            logger.info("Class does not contain fields");
        } else {
            Stream.of(fields).forEach(e -> {
                e.setAccessible(true);
                try {
                    logger.info("Field: " + e.getType() + " " + e.getName() + " = " + e.get(object));
                } catch (IllegalAccessException e1) {
                    logger.info(e1.getMessage());
                }
            });
        }
    }

    private void getAllConstructors() {
        logger.info("Information about constructor: ");
        Constructor[] constructors = c.getDeclaredConstructors();
        if (constructors.length == 0) {
            logger.info("Class does not contain constructors");
        } else {
            Stream.of(constructors).forEach(e -> {
                logger.info("Count modifiers = " + e.getModifiers() + "; " + e.toString());
            });
        }
    }

    private void getAllMethods() {
        logger.info("Information about methods: ");
        Method[] methods = c.getDeclaredMethods();
        if (methods.length == 0) {
            logger.info("Class does not contain methods");
        } else {
            Stream.of(methods).forEach(e -> {
                logger.info(e.getReturnType() + " " + e.getName());
            });
        }
    }

    private void getAllAnnotation() {
        logger.info("Information about Annotation: ");
        Annotation[] annotations = c.getDeclaredAnnotations();
        if (annotations.length == 0) {
            logger.info("Class does not contain annotations");
        } else {
            Stream.of(annotations).forEach(e -> {
                logger.info(e.annotationType().getDeclaredFields());
            });
        }
    }

}

package com.dubilok.model.ex_4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProblemsFourth {

    private static Logger logger = LogManager.getLogger(ProblemsFourth.class);

    public int getSum(int a, int b) {
        return a + b;
    }

    public double getDoubleValue(double value) {
        return value;
    }

    public void printSomething() {
        logger.info("Print something");
    }
}

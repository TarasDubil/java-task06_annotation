package com.dubilok.model.ex_6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProblemsSixth {

    private static Logger logger = LogManager.getLogger(ProblemsSixth.class);

    public void myMethod(String a, int ... args) {
        logger.info("My method with String a, int ... args");
    }

    public void myMethod(String... args) {
        logger.info("My method with String... args");
    }

}

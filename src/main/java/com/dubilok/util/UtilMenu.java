package com.dubilok.util;

import com.dubilok.view.Printable;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

public class UtilMenu {
    private static void outputMenu(Map<String, String> menu) {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public static void show(BufferedReader bufferedReader, Map<String, String> menu, Map<String, Printable> methodsMenu) {
        String keyMenu = null;
        do {
            outputMenu(menu);
            System.out.println("Please, select menu point.");
            try {
                keyMenu = bufferedReader.readLine().toUpperCase();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}

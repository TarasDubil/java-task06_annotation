package com.dubilok.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public interface Controller {

    void showSecondProblem();

    void showThirdProblem();

    void showFourthProblem() throws IllegalAccessException, IOException, InvocationTargetException;

    void showFifthProblem() throws IllegalAccessException, NoSuchFieldException, IOException;

    void showSixthProblem() throws NoSuchMethodException, IOException, IllegalAccessException, InvocationTargetException;

    void showSeventhProblem() throws IllegalAccessException;

}

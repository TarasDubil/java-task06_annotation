package com.dubilok.controller;

import com.dubilok.service.Manager;
import com.dubilok.service.ManagerImpl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class ControllerImpl implements Controller {

    private Manager manager;

    public ControllerImpl() {
        this.manager = new ManagerImpl();
    }

    @Override
    public void showSecondProblem() {
        manager.showSecondProblem();
    }

    @Override
    public void showThirdProblem() {
        manager.showThirdProblem();
    }

    @Override
    public void showFourthProblem() throws IllegalAccessException, IOException, InvocationTargetException {
        manager.showFourthProblem();
    }

    @Override
    public void showFifthProblem() throws IllegalAccessException, NoSuchFieldException, IOException {
        manager.showFifthProblem();
    }

    @Override
    public void showSixthProblem() throws NoSuchMethodException, IOException, IllegalAccessException, InvocationTargetException {
        manager.showSixthProblem();
    }

    @Override
    public void showSeventhProblem() throws IllegalAccessException {
        manager.showSeventhProblem();
    }
}

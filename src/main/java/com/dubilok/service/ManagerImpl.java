package com.dubilok.service;

import com.dubilok.model.annotation.Person;
import com.dubilok.model.annotation.Show;
import com.dubilok.model.ex_4.ProblemsFourth;
import com.dubilok.model.ex_6.ProblemsSixth;
import com.dubilok.model.ex_7.Dog;
import com.dubilok.model.ex_7.DogManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ManagerImpl implements Manager {

    private static Logger logger = LogManager.getLogger(ManagerImpl.class);
    private Person person = new Person();
    private Class clazz = person.getClass();
    private BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private DogManager dogManager = new DogManager(new Dog());
    private ProblemsSixth problemsSixth = new ProblemsSixth();
    private ProblemsFourth problemsFourth = new ProblemsFourth();

    @Override
    public void showSecondProblem() {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().equals(Show.class)) {
                    try {
                        logger.info(field.getType() + " " + field.getName() + " = " + field.get(person));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void showThirdProblem() {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().equals(Show.class)) {
                    Show show = field.getAnnotation(Show.class);
                    try {
                        logger.info(field.getType() + " " + field.getName() + " = " + field.get(person) + "; Annotation value: " + show.value());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void showFourthProblem() throws InvocationTargetException, IllegalAccessException, IOException {
        Method[] methods = ProblemsFourth.class.getDeclaredMethods();
        for (Method method : methods) {
            Object[] parameterTypes = method.getParameterTypes();
            if (parameterTypes.length == 0) {
                method.invoke(problemsFourth);
            }
            if (method.getName().equals("getSum")) {
                logger.info("Enter your first Integer value: ");
                int first = Integer.parseInt(bufferedReader.readLine());
                logger.info("Enter your second Integer value: ");
                int second = Integer.parseInt(bufferedReader.readLine());
                Object invoke = method.invoke(problemsFourth, first, second);
                logger.info("Sum: " + invoke);
            }
            if (method.getName().equals("getDoubleValue")) {
                logger.info("Enter your Double value: ");
                double value = Double.parseDouble(bufferedReader.readLine());
                Object invoke = method.invoke(problemsFourth, value);
                logger.info("Get Value: " + invoke);
            }
        }
    }

    @Override
    public void showFifthProblem() throws IOException, IllegalAccessException, NoSuchFieldException {
        Field field = clazz.getDeclaredField("name");
        field.setAccessible(true);
        logger.info("Old value: " + field.get(person));
        logger.info("Enter your String value: ");
        String value = bufferedReader.readLine();
        field.set(person, value);
        logger.info("New value: " + field.get(person));
    }

    @Override
    public void showSixthProblem() throws InvocationTargetException, NoSuchMethodException, IllegalAccessException, IOException {
        logger.info("Enter your string value: ");
        invokeMyMethodWithString(bufferedReader.readLine());
        logger.info("Enter your Integer value: ");
        Integer value = Integer.valueOf(bufferedReader.readLine());
        logger.info("Enter your String value: ");
        String valueString = bufferedReader.readLine();
        invokeMyMethodWithStringAndInt(valueString, value);
    }

    private void invokeMyMethodWithString(String... values) throws NoSuchMethodException, IOException, InvocationTargetException, IllegalAccessException {
        Method method = ProblemsSixth.class.getDeclaredMethod("myMethod", String[].class);
        method.invoke(problemsSixth, new Object[]{values});
    }

    private void invokeMyMethodWithStringAndInt(String stringValue, int... intValues) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = ProblemsSixth.class.getDeclaredMethod("myMethod", String.class, int[].class);
        method.invoke(problemsSixth, stringValue, intValues);
    }

    @Override
    public void showSeventhProblem() throws IllegalAccessException {
        dogManager.getInfoAboutClass();
    }
}

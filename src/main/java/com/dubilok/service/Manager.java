package com.dubilok.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public interface Manager {
    void showSecondProblem();

    void showThirdProblem();

    void showFourthProblem() throws InvocationTargetException, IllegalAccessException, IOException;

    void showFifthProblem() throws IOException, IllegalAccessException, NoSuchFieldException;

    void showSixthProblem() throws InvocationTargetException, NoSuchMethodException, IllegalAccessException, IOException;

    void showSeventhProblem() throws IllegalAccessException;
}

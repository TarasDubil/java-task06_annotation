package com.dubilok.view;

import com.dubilok.controller.Controller;
import com.dubilok.controller.ControllerImpl;
import com.dubilok.util.UtilMenu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ViewImpl implements View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public ViewImpl() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print fields in the class that were annotate by annotation.");
        menu.put("2", "  2 - Print annotation value");
        menu.put("3", "  3 - Invoke three methods");
        menu.put("4", "  4 - Set value into field not knowing its type.");
        menu.put("5", "  5 - Invoke myMethod(String a, int ... args) and myMethod(String … args).");
        menu.put("6", "  6 - show all information about Dog Class.");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton2);
        methodsMenu.put("2", this::pressButton3);
        methodsMenu.put("3", this::pressButton4);
        methodsMenu.put("4", this::pressButton5);
        methodsMenu.put("5", this::pressButton6);
        methodsMenu.put("6", this::pressButton7);
    }

    private void pressButton2() {
        controller.showSecondProblem();
    }

    private void pressButton3() {
        controller.showThirdProblem();
    }

    private void pressButton4() {
        try {
            controller.showFourthProblem();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void pressButton5() {
        try {
            controller.showFifthProblem();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton6() {
        try {
            controller.showSixthProblem();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void pressButton7() {
        try {
            controller.showSeventhProblem();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    public void show() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
